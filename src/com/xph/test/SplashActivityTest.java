package com.xph.test;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.ListView;

import com.xph.activities.MainActivity;
import com.xph.activities.SplashActivity;
import com.xph.google.GCMConnector;

public class SplashActivityTest extends
		ActivityInstrumentationTestCase2<com.xph.activities.SplashActivity> {

	public SplashActivityTest(Class<SplashActivity> activityClass) {
		super(activityClass);
		// TODO Auto-generated constructor stub
	}

	private SplashActivity mSplashActivity;
	public static GCMConnector gcmConnector;
	//private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private String[] mDrawerTitles;
	private ActionBar mActionBar;
	
	public SplashActivityTest() {
		super(com.xph.activities.SplashActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();

//		mSplashActivity = getActivity();
        mSplashActivity = getActivity();
        
        
//		mDrawerLayout = (DrawerLayout) mActivity
//				.findViewById(com.xph.R.id.drawer_layout);
//		mDrawerList = (ListView) mActivity
//				.findViewById(com.xph.R.id.listview_drawer);
//		mDrawerTitles = new String[] {
//				mActivity.getString(com.xph.R.string.my_week),
//				mActivity.getString(com.xph.R.string.cookbook),
//				mActivity.getString(com.xph.R.string.login),
//				mActivity.getString(com.xph.R.string.households),
//				mActivity.getString(com.xph.R.string.settings) };
//		mActionBar = mActivity.getSupportActionBar();
	}

	public void test0Preconditions() {
		getInstrumentation().waitForIdleSync();
		assertNotNull("mSplashActivity is null", mSplashActivity);
//		assertNotNull("mDrawerLayout is null", mDrawerLayout);
//		assertNotNull("mDrawerList is null", mDrawerList);
//		assertNotNull("mDrawerTitles is null", mDrawerTitles);
//		assertNotNull("mActionBar is null", mActionBar);
		
	}

	public void testTabIndex() {
		getInstrumentation().waitForIdleSync();
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(MainActivity.class.getName(), null, false);
		mMainActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				mMainActivity.getActionBar().setSelectedNavigationItem(2);
				mMainActivity.finish();
			}
		});
		getInstrumentation().waitForIdleSync();
		mMainActivity = getActivity();
		assertEquals("incorrect tab selected", mMainActivity.getActionBar()
				.getSelectedNavigationIndex(), 2);
	}

//	public void drawerIndex(final int index) {
//		getInstrumentation().waitForIdleSync();
//		mMainActivity = getActivity();
//		mDrawerList = (ListView) mSplashActivity
//		.findViewById(com.xph.R.id.listview_drawer);
//		mMainActivity.runOnUiThread(new Runnable() {
//			@Override
//			public void run() {
//				mDrawerList.setItemChecked(index, true);
//				mDrawerList.performClick();
//				mMainActivity.finish();
//			}
//		});
//		getInstrumentation().waitForIdleSync();
//		mMainActivity = getActivity();
//		mDrawerList = (ListView) mMainActivity
//				.findViewById(com.xph.R.id.listview_drawer);
//		assertEquals("incorrect drawer item selected",
//				mDrawerList.getCheckedItemPosition(), index);
//	
//	}
//	public void testDrawerIndex() {
//		drawerIndex(0);
//		drawerIndex(1);
//		drawerIndex(2);
//		drawerIndex(3);
//		drawerIndex(4);
//	}
	
//	 public void testRecipePopup() {
//	 getInstrumentation().waitForIdleSync();
//	 mActivity.runOnUiThread(new Runnable() {
//		
//		@Override
//		public void run() {
//			mDrawerList.getOnItemClickListener().onItemClick(null, null, 0, 0);
//			mActivity.getActionBar().setSelectedNavigationItem(0);
//		}
//	 });
//	 getInstrumentation().waitForIdleSync();
//	 ListView mHomescreenList = (ListView) mActivity.findViewById(com.xph.R.id.homescreen_list);
//	 assertNotNull(mHomescreenList);
//	 final  LinearLayout ll = (LinearLayout) mActivity.findViewById(R.id.homescreen_imgBtnLinLayout);
//	 assertNotNull(ll);
//	 mActivity.runOnUiThread(new Runnable() {
//	 @Override
//	 public void run() {
//		 RecipeButtonListItem mImageButton = (RecipeButtonListItem) ll.findViewById(com.xph.R.id.homescreen_imgBtn);
//		 assertNotNull(mImageButton);
//		 mImageButton.performClick();
//	 }
//	 });
//	 getInstrumentation().waitForIdleSync();
//	 }
	
//	public void testSettings() {
//		getInstrumentation().waitForIdleSync();
//		mActivity.runOnUiThread(new Runnable() {
//			@Override
//			public void run() {
//				mDrawerList.getOnItemClickListener().onItemClick(null, null, 4, 0);
//			}
//		});
//		getInstrumentation().waitForIdleSync();
//		mActivity.runOnUiThread(new Runnable(
//				) {
//			
//			@Override
//			public void run() {
//				CheckBox cbView1 = (CheckBox) mActivity
//						.findViewById(com.xph.R.id.settings_homescreen_meal);
//				CheckBox cbView2 = (CheckBox) mActivity
//						.findViewById(com.xph.R.id.settings_homescreen_recipe);
//				CheckBox cbView3 = (CheckBox) mActivity
//						.findViewById(com.xph.R.id.settings_homescreen_weekplan);
//				
//				CheckBox cb1 = (CheckBox) cbView1
//						.findViewById(com.xph.R.id.settings_checkbox);
//				CheckBox cb2 = (CheckBox) cbView2
//						.findViewById(com.xph.R.id.settings_checkbox);
//				CheckBox cb3 = (CheckBox) cbView3
//						.findViewById(com.xph.R.id.settings_checkbox);
//				
//				cb1.setChecked(false);
//				cb2.setChecked(false);
//				cb3.setChecked(false);
//				mActivity.onBackPressed();
//			}
//		});
//		getInstrumentation().waitForIdleSync();
//		mActivity.runOnUiThread(new Runnable() {
//			@Override
//			public void run() {
//				mDrawerList.getOnItemClickListener().onItemClick(null, null, 4, 0);
//			}
//		});
//		getInstrumentation().waitForIdleSync();
//		mActivity.runOnUiThread(new Runnable() {
//			@Override
//			public void run() {
//				SettingsCheckboxView cbView1 = (SettingsCheckboxView) mActivity
//						.findViewById(com.xph.R.id.settings_homescreen_meal);
//				SettingsCheckboxView cbView2 = (SettingsCheckboxView) mActivity
//						.findViewById(com.xph.R.id.settings_homescreen_recipe);
//				SettingsCheckboxView cbView3 = (SettingsCheckboxView) mActivity
//						.findViewById(com.xph.R.id.settings_homescreen_weekplan);
//				final CheckBox cb1 = (CheckBox) cbView1
//						.findViewById(com.xph.R.id.settings_checkbox);
//				final CheckBox cb2 = (CheckBox) cbView2
//						.findViewById(com.xph.R.id.settings_checkbox);
//				final CheckBox cb3 = (CheckBox) cbView3
//						.findViewById(com.xph.R.id.settings_checkbox);
//				
//				assertFalse("meal checkbox", cb1.isChecked());
//				assertFalse("recipe checkbox", cb2.isChecked());
//				assertFalse("weekplan checkbox", cb3.isChecked());
//				
//				Session.getInstance().updateUser(new PersistenceCallback<User>() {
//					@Override
//					public void dataAvailable(PersistenceError error, List<User> entities) {
//						if (error == null) {
//							Persistence.getInstance().findAssociated(Session.getInstance().getUser(), User.ASSOC_SETTINGS,
//									new PersistenceCallback<Settings>() {
//										@Override
//										public void dataAvailable(PersistenceError error, List<Settings> entities) {
//											if (error == null) {
//												Settings settings = entities.get(0);
//												assertEquals("CheckBoxMeal is checked",cb1.isChecked(), settings.getMeal());
//												assertEquals("CheckBoxRecipe is checked", cb2.isChecked(), settings.getRecipe());
//												assertEquals("CheckBoxWeekplan is checked", cb3.isChecked(), settings.getWeekplan());
//											} else {
//												Toast.makeText(getActivity(), "Fehler beim Einstellungen laden", Toast.LENGTH_LONG).show();
//											}
//										}
//									});
//						} else {
//							Toast.makeText(getActivity(), "Fehler beim Benutzer laden", Toast.LENGTH_LONG).show();
//						}
//					}
//				});
//			}
//		});
//		
//	}
	
	
//	public void testFavorits(){
//		
//	}
	}


